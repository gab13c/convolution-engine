/* ----------------------------------------------------------------
* This module is the MAC of the process engine.
* The inputs will be the weight and pixel value from the memory.
* The inputs will be multiplied and then summed 9 times forming the
* complete kernel and the output will be the summed subframe of [15:0] bits.
 
* @author - Nathan Kendall
* @date - 10/19/19
*---------------------------------------------------------------- */
`include "conv_constraints.v"

module MAC #(parameter status_reg_size = `status_reg_size, frame_size = `frame_size, kernel_size = `kernel_size)
		(	input clk,		input reset,
			input [7:0] pix_val,	input [7:0] weight,
			input start,	output reg [15:0] subframe_out);

//	reg [15:0] conv;
 	
	always@(posedge clk, posedge reset)
		begin	
			if (reset)
				begin
//					conv <= 16'b0;
					subframe_out <= 16'b0;
				end
			else	
				begin
					if (start)
						begin
//							conv <= pix_val * weight;
							subframe_out <= subframe_out + pix_val * weight;
						end
				end
		end		
endmodule
