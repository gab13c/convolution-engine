/*
* synchronous SRAM Module
* read and write are active high
* @author Grant Brown
* @date 10/18/19
*/

module SRAM (dataIn, dataOut, addr, write, read, Clk);
	
	//this size is for 26 x 26 x 8 bit memory
	parameter data_Size = 8;
	parameter address_Size = 7;
	parameter depth = 100;
	
	input wire [data_Size-1:0] dataIn;
	output reg [data_Size-1:0] dataOut;
	input [address_Size-1:0] addr;
	input write, read, Clk;
	
	reg [data_Size -1:0] SRAM [depth-1:0];
	integer i;
	
	always @ (posedge Clk) begin
		if (write == 1'b1 && read == 1'b0) 
				SRAM[addr] <= dataIn;
		else if (read == 1'b1 && write == 1'b0) 
				dataOut <= SRAM[addr];
		end
endmodule
				
			
			
		
	
	
	
	
