module tAGU;

	parameter data_index_bits = 5;
	parameter filter_index_bits = 2;
	parameter data_addr_bits = 13;
	parameter filter_addr_bits = 13;
	reg [data_index_bits - 1: 0] w;
	reg [data_index_bits - 1: 0] h;
	reg [filter_index_bits - 1: 0] r;
    reg [filter_index_bits - 1: 0] s;
  	// address for output element
	wire [data_addr_bits - 1: 0] oaddr;
	// address for input element
	wire [data_addr_bits - 1: 0] iaddr;
	// address for filter element
	wire [data_addr_bits - 1: 0] faddr;

    AGU mAGU(.w(w), .h(h), .r(r), .s(s), .oaddr(oaddr),
             .iaddr(iaddr), .faddr(faddr));
    integer i, j, p, q;
	 
	 localparam Wo = 24;
	 localparam Wi = 26;
	 localparam R = 3;
	 localparam S = 3; 
	

    always begin
        $display("h w s r  oaddr iaddr faddr");
        for (i = 0; i < 4; i = i + 1) begin
            for (j = 0; j < 4; j = j + 1) begin
                for (p = 0; p < S; p = p + 1) begin
                    for (q = 0; q < R; q = q + 1) begin
                        h <= i;
                        w <= j;
                        s <= p;
                        r <= q;
                        $display("%d %d %d %d %d", h, w, s, r, oaddr, iaddr, faddr);
							#2;
							if(oaddr != (h * Wo) + w) begin
								$display("oaddr computed incorrectly, should be: %d, but is: %d",((i * Wo) + j), oaddr);
								$finish;
							end
							if(iaddr != (h+s) * Wi + (w+r)) begin
									$display("iaddr computed incorrectly, should be: %d, but is: %d",((i+p) * Wi + (j+q)), iaddr);
									$finish;
                    end
						  if(faddr != (s * R + r)) begin
									$display("faddr computed incorrectly, should be: %d, but is: %d",(p * R + q), faddr);
									$finish;
						  end
						end
                end
            end
        end
    end
endmodule