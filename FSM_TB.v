/* --------------------------------------------------------------------------------------------------
* Testbench file for FSM for convolutional engine
* --------------------------------------------------------------------------------------------------*/

`timescale 1ns / 1ns 

module FSM_TB();

 localparam data_Size = 5;
 localparam kernel_size = 2;
 reg start_conv, clk, reset; 
 wire wr_output_memory, wr_input_memory, start_PE, conv_complete;		
  
 wire [3:0] clear, increment; 
 reg [data_Size-1: 0] w; 
 reg [data_Size-1: 0] h;
 reg [kernel_size-1: 0] r; 
 reg [kernel_size-1: 0] s;
 integer i;
 wire [2:0] curr_state;

 FSM fsm (.start_conv(start_conv), .clk(clk), .reset(reset),
				.clear(clear), .increment(increment),
				.wr_output_memory(wr_output_memory), .wr_input_memory(wr_input_memory), 
				.start_PE(start_PE), .conv_complete(conv_complete),
				.w_status(w), .h_status(h), .r_status(r), .s_status(s), .curr_state(curr_state));
				
		initial
			begin
			$monitor("At time=%0t Clear=%b, Increment=%b, Output Write=%b, Input Read=%b, Start PE=%b, Conv Complete=%b", $time, clear, increment, wr_output_memory, wr_input_memory, start_PE, conv_complete);
				w = 0;
				h = 0;
				r = 0;
				s = 0;
			end
	
		always 
			#10 clk = !clk;
			
		always @ (posedge clk) begin
			case(increment)
				4'h8: #2 r <= r + 1;
				4'h4: #2 s <= s + 1;
				4'h2: #2 w <= w + 1;
				4'h1: #2 h <= h + 1;
				4'hC: begin
							#2 r <= r + 1;
							s <= s + 1;
						end
			endcase
		end
		
		always @ (posedge clk) begin
			case(clear)
				4'h8: #2 r <= 0;
				4'h4: #2 s <= 0;
				4'h2: #2 w <= 0;
				4'h1: #2 h <= 0;
				4'hC: begin
							#2 r <= 0;
							s <= 0;
						end
			endcase
		end
				
				

		initial 
			begin
				clk = 1'b0;
				$display("State Should be A");
				reset <= 1'b1;
			   #10 reset <= 1'b0;
				start_conv <= 1'b1;
				@(posedge clk) start_conv <= 1'b0;
				$display("State Should be B");
				#20 $display("State should be E");
				#10 $display("Going Back to State B");
			end
				
				
endmodule