/*---------------------------------------------------------------------
*
* Address registers which are used to load weight, input and output memory onto the SRAMs, also to load data off output SRAM
*
* incr = clk
----------------------------------------------------------------------*/

`include "conv_constraints.v"

module addr_reg #(parameter data_addr_bits = `data_addr_bits)
					  ( input clk, input reset,
						output reg [data_addr_bits - 1:0] addr);
	
	always @ (posedge clk or posedge reset) begin
			if(reset)
				addr <= 0;
			else
				addr <= addr + 1;
	end
endmodule