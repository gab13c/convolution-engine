/* ---------------------------------------------------------------------------------
*
* Constraints File containing Macros used throughout project
*
-----------------------------------------------------------------------------------*/

`define input_bits 8
`define output_bits 16
`define status_reg_size 4
`define kernel_size 2
`define frame_size 4
`define data_addr_bits 7
`define filter_addr_bits 4
`define Wo 8
`define Ho 8
`define Wi 10
`define Hi 10
`define R 3
`define S 3
