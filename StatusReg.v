/* ----------------------------------------------------------------
* This module is the Status Register of the u-convolution chip.
* The inputs will be the increment and clear signals from the FSM module. 
* This module keeps track of the current positions of the pixel within 
* the kernel and pixel position of the input frame both of which are 
* output to be used by the FSM and ADU. 
 
* @author - Nathan Kendall
* @date - 10/31/19
*---------------------------------------------------------------- */
`include "conv_constraints.v"

module StatusReg #(parameter status_reg_size = `status_reg_size, frame_size = `frame_size, kernel_size = `kernel_size)
		(input clk,				input reset,
			input [status_reg_size-1:0] inc,	input [status_reg_size-1:0] clear,
			output reg [frame_size-1:0] w,		output reg [frame_size-1:0] h,
			output reg [kernel_size-1:0] r,		output reg [kernel_size-1:0] s);	
	
	always@(posedge clk, posedge reset)
		begin	
			if (reset)
				begin
					h <= 5'b0;
					w <= 5'b0;
					s <= 2'b0;
					r <= 2'b0;
				end
			else	
				begin
		h <= (h + 1'b1*((4'b0001&inc)==4'b0001))*(1'b1*((4'b0001&clear)!=4'b0001));
		w <= (w + 1'b1*((4'b0010&inc)==4'b0010))*(1'b1*((4'b0010&clear)!=4'b0010));
		s <= (s + 1'b1*((4'b0100&inc)==4'b0100))*(1'b1*((4'b0100&clear)!=4'b0100));
		r <= (r + 1'b1*((4'b1000&inc)==4'b1000))*(1'b1*((4'b1000&clear)!=4'b1000));
					
/*					casex(inc)
						4'bxxx1:	h <= h + 1'b1;
						4'bxx1x:	w <= w + 1'b1;
						4'bx1xx:	s <= s + 1'b1;
						4'b1xxx:	r <= r + 1'b1;	//4'b1000
					endcase
					casex(clear)
						4'bxxx1:	h <= 5'b0;
						4'bxx1x:	w <= 5'b0;
						4'bx1xx:	s <= 2'b0;
						4'b1xxx:	r <= 2'b0;		//4'b1000
					endcase
*/				end
		end		
endmodule
