//Top_level TB
`include "conv_constraints.v"

module ucell_tb ();

localparam input_bits = `input_bits, output_bits = `output_bits,
			  kernel_size = `kernel_size, frame_size = `frame_size,
			  status_reg_size = `status_reg_size, data_addr_bits = `data_addr_bits,
			  filter_addr_bits = `filter_addr_bits;

reg clk, reset, start_conv, e_oaddr_reset, e_oaddr_inc, e_iaddr_reset, e_iaddr_inc, e_faddr_reset, e_faddr_inc;
reg [input_bits-1:0] i_bus, f_bus;
wire [output_bits-1:0] o_bus;
wire conv_complete;

	 wire wr_output_memory;
	 wire [output_bits-1:0] out_pixel;
   wire [input_bits-1:0] in_pixel;
   wire [data_addr_bits-1:0] 	oaddr;
   wire [data_addr_bits-1:0] 	iaddr;
   wire [filter_addr_bits-1:0]  faddr;
   wire wr_input_memory;
   wire [frame_size - 1:0] w;
   wire [frame_size - 1:0] h;
   wire [input_bits-1:0] weight;
   wire [status_reg_size - 1:0] inc;
   wire [status_reg_size - 1:0] clear;
   wire [kernel_size - 1:0] r;
   wire [kernel_size - 1:0] s;
   wire start_PE;
	wire [2:0] curr_state;

ucell top_level (.clk(clk), .reset(reset), .start_conv(start_conv), .e_oaddr_reset(e_oaddr_reset),
					  .e_oaddr_inc(e_oaddr_inc), .e_iaddr_reset(e_iaddr_reset), .e_iaddr_inc(e_iaddr_inc),
					  .e_faddr_reset(e_faddr_reset), .e_faddr_inc(e_faddr_inc), .i_bus(i_bus),
					  .f_bus(f_bus), .o_bus(o_bus), .conv_complete(conv_complete));
					  
integer i;
	initial begin
		reset <= 0;
		clk <= 0;
		e_faddr_reset <= 0;
		e_iaddr_reset <= 0;
		e_oaddr_reset <= 0;
		start_conv <= 0;
		e_iaddr_inc <= 0;
		e_faddr_inc <= 0;
	end

	always
	#6 clk = !clk;

	initial begin
	#1; //allows other initial block to run first
	reset <= 1;
	e_faddr_reset <= 1;
	e_iaddr_reset <= 1;
	e_oaddr_reset <= 1;
	#10;
	reset <= 0;
	e_faddr_reset <= 0;
	e_iaddr_reset <= 0;
	e_oaddr_reset <= 0;
	#10;
	i_bus <= 0;
	f_bus <= 0;
	#5; //initial delay
		for(i = 0; i < 100; i = i+ 1) begin
			//so i/f bus is set with data and we increment the addr which is used as clock for writing the data
			e_iaddr_inc <= 1;
			#6;
			e_iaddr_inc = 0;
			#3 i_bus <= i+1;
			#1	;
		end
		i <= 0;
	#5;
		for(i = 0; i < 9; i = i+ 1) begin
			//so i/f bus is set with data and we increment the addr which is used as clock for writing the data
			e_faddr_inc <= 1;
			#6;
			e_faddr_inc = 0;
			#3 f_bus <= i+1;
			#1	;
		end
		i <= 0;
	#5
	$display("%d %d %d", top_level.inputs.SRAM[0], top_level.inputs.SRAM[1], top_level.inputs.SRAM[2]);
	$display("%d %d %d", top_level.inputs.SRAM[10], top_level.inputs.SRAM[10+1], top_level.inputs.SRAM[10+2]);
	$display("%d %d %d", top_level.inputs.SRAM[20], top_level.inputs.SRAM[20+1], top_level.inputs.SRAM[20+2]);
	$display("%d %d %d", top_level.weights.SRAM[0], top_level.weights.SRAM[1], top_level.weights.SRAM[2]);
	$display("%d %d %d", top_level.weights.SRAM[3], top_level.weights.SRAM[3+1], top_level.weights.SRAM[3+2]);
	$display("%d %d %d", top_level.weights.SRAM[6], top_level.weights.SRAM[6+1], top_level.weights.SRAM[6+2]);
	start_conv <= 1;
        $monitor("t=%3d Write_addr=%d \n",$time,top_level.outputs.addr);
	#1000;
	$display("%d", top_level.outputs.SRAM[0]);
	$display("done sim!");
	end
	always @ (posedge conv_complete) 
		start_conv <= 0;
endmodule
