//Top_level design
//Start_conv needs to be 0 while conv is not in progress, 1 until reaching conv_complete signal
// reset goes to all non SRAM modules

`include "conv_constraints.v"
`include "SRAM.v"
`include "addr_reg.v"
`include "StatusReg.v"
`include "AGU.v"
`include "mac.v"
`include "FSM.v"

module ucell #(parameter input_bits = `input_bits, output_bits = `output_bits,
								  kernel_size = `kernel_size, frame_size = `frame_size,
								  status_reg_size = `status_reg_size, data_addr_bits = `data_addr_bits,
								  filter_addr_bits = `filter_addr_bits)
				  (clk, reset, start_conv, i_bus, o_bus, f_bus, e_oaddr_reset,
					e_oaddr_inc, e_iaddr_reset, e_iaddr_inc, e_faddr_reset, e_faddr_inc, conv_complete);
					
   input clk, reset, start_conv, e_oaddr_reset, e_oaddr_inc,
	 e_iaddr_reset, e_iaddr_inc, e_faddr_reset, e_faddr_inc;
   input [input_bits-1:0] i_bus;
   input [input_bits-1:0] f_bus;
   output [output_bits-1:0] o_bus;
   output conv_complete;
	 wire wr_output_memory;
	 wire [output_bits-1:0] out_pixel;
	 wire [input_bits-1:0] in_pixel;
    wire [5:0] 	oaddr;
    wire [data_addr_bits-1:0] 	iaddr;
    wire [filter_addr_bits-1:0]  faddr;
	 wire wr_input_memory;
	 wire [frame_size - 1:0] w;
    wire [frame_size - 1:0] h;
	 wire [input_bits-1:0] weight;
    wire [status_reg_size - 1:0] inc;
    wire [status_reg_size - 1:0] clear;
    wire [kernel_size - 1:0] r;
    wire [kernel_size - 1:0] s;
    wire start_PE;
   // e_[oif]addr are registers for loading the memories by
   // external entity, they will increment on every clock pulse of
   // e_[oif]addr_inc signal and they reset on [oif]addr_reset signal
   // WARN: asserting [oif]addr_reset signals and/or sending posedge on _inc 
   // signals while start_conv asserted is undefined behavior
   wire [data_addr_bits-1:0] e_oaddr;
   wire [data_addr_bits-1:0] e_iaddr;
   wire [filter_addr_bits-1:0] e_faddr;

   wire [data_addr_bits-1:0]  agu_oaddr;
   wire [data_addr_bits-1:0]  agu_iaddr;
   wire [filter_addr_bits-1:0] agu_faddr;
	
   wire external_access;
   assign external_access = ~start_conv;
	wire clear_mac; //produced by fsm
	wire reset_mac; //clear mac global or by FSM

   // Clock for memory is either external clock, who will fill in buffers
   // or the primary clock which will feed the data to mac
   wire m_clk_i, m_clk_o, m_clk_f; // m -> memory
   assign m_clk_i = start_conv ? clk : e_iaddr_inc;
   assign m_clk_o = start_conv ? clk : e_oaddr_inc;
   assign m_clk_f = start_conv ? clk : e_faddr_inc;
   assign oaddr = start_conv ? agu_oaddr : e_oaddr;
   assign iaddr = start_conv ? agu_iaddr : e_iaddr;
   assign faddr = start_conv ? agu_faddr : e_faddr;
	
	addr_reg i_addr (.clk(e_iaddr_inc), .reset(e_iaddr_reset),
								.addr(e_iaddr));
								
	
	addr_reg o_addr (.clk(e_oaddr_inc), .reset(e_oaddr_reset),
								.addr(e_oaddr));
	
	defparam f_addr.data_addr_bits = 4;
	addr_reg f_addr (.clk(e_faddr_inc), .reset(e_faddr_reset),
								.addr(e_faddr));



   AGU agu(.w(w), .h(h), .r(r), .s(s), .oaddr(agu_oaddr), .iaddr(agu_iaddr),
	   .faddr(agu_faddr));

   defparam weights.address_Size = filter_addr_bits;
   SRAM weights(.dataIn(f_bus), .dataOut(weight), .addr(faddr), .write(external_access),
		.read(wr_input_memory), .Clk(m_clk_f));
		
   SRAM inputs(.dataIn(i_bus), .dataOut(in_pixel), .addr(iaddr), .write(external_access),
	       .read(wr_input_memory), .Clk(m_clk_i));
	
	defparam outputs.data_Size = 16;
	defparam outputs.address_Size = 6;
	defparam outputs.depth = 64;
   SRAM outputs(.dataIn(out_pixel), .dataOut(o_bus), .addr(oaddr),
		.write(wr_output_memory), .read(external_access), .Clk(m_clk_o));
		
   StatusReg sreg(.clk(clk), .reset(reset), .inc(inc), .clear(clear),
		      .w(w), .h(h), .r(r), .s(s));
				
   FSM fsm(.start_conv(start_conv), .clk(clk), .reset(reset), .clear(clear),
	   .increment(inc), .wr_output_memory(wr_output_memory),
	   .wr_input_memory(wr_input_memory), .start_PE(start_PE),
	   .conv_complete(conv_complete), .w_status(w),.h_status(h),
	   .r_status(r), .s_status(s), .clear_mac(clear_mac));
		
	assign reset_mac = clear_mac | reset;
   MAC mac(.clk(clk),.reset(reset_mac),.pix_val(in_pixel),.weight(weight),.start(start_PE),
	   .subframe_out(out_pixel));
		
endmodule
