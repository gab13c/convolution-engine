/* ----------------------------------------------------------------
* This module is the Status Register of the u-convolution chip.
* The inputs will be the increment and clear signals from the FSM module. 
* This module keeps track of the current positions of the pixel within 
* the kernel and pixel position of the input frame both of which are 
* output to be used by the FSM and ADU. 
 
* @author - Miheer Vaidya
* @date - 10/19/19
*---------------------------------------------------------------- */

`include "conv_constraints.v"

module AGU #(parameter  Wo = `Wo, Ho = `Ho,
								Wi = `Wi, Hi = `Hi,
							  data_index_bits = `frame_size, filter_index_bits = `kernel_size,
							  data_addr_bits = `data_addr_bits, filter_addr_bits = `filter_addr_bits,
							  R = `R, S = `S)
				(w, h, r, s, oaddr, iaddr, faddr);

	input [data_index_bits - 1: 0] w;
	input [data_index_bits - 1: 0] h;
	input [filter_index_bits - 1: 0] r;
	input [filter_index_bits - 1: 0] s;
	// address for output element
	output [data_addr_bits - 1: 0] oaddr;
	// address for input element
	output [data_addr_bits - 1: 0] iaddr;
	// address for filter element
	output [filter_addr_bits - 1: 0] faddr;
	assign oaddr = h * Wo + w;
	assign iaddr = (h+s) * Wi + (w+r);
	assign faddr = s * R + r;
endmodule
