/* -------------------------------------------------------------------------------------
* This Moore FSM is responsible for operation of the convolution engine block,
* The main operaton of it is the ensure that the status register is correctly pointing
* to the right pixel needed for the current convolution. It will be responsible for the 
* h_sync and v_sync of the master pixel frame
* --------------------------------------------------------------------------------------*/

/* -------------------------------------------------------------------------------------
* Variable Description:
* increment[3] = r, increment[2] = s, increment[1] = w, increment[0] = h
* wr_input_memory = Loads the variable from memory to MAC
* -------------------------------------------------------------------------------------*/

`include "conv_constraints.v"

module FSM #(parameter status_reg_size = `status_reg_size, frame_size = `frame_size, kernel_size = `kernel_size,
					W_Size = `Wi, H_Size = `Hi)
				(input start_conv, input clk, input reset,
				output reg [status_reg_size - 1:0] clear, output reg [status_reg_size - 1:0] increment,
				output reg wr_output_memory, output reg wr_input_memory, 
				output reg start_PE, output reg conv_complete, output reg clear_mac,
				input [frame_size - 1: 0] w_status, input [frame_size - 1: 0] h_status, 
				input [kernel_size - 1: 0] r_status, input [kernel_size - 1: 0] s_status);
				
		localparam A = 0, B = 1, D = 2, E = 3, G = 4, I = 5, J = 6;
		
		localparam R = 3, S = 3, W = W_Size, H = H_Size;
		
		reg [2:0] next_state;
		reg [2:0] curr_state;
	
		//combinational block
		always @ (*) begin
			case(curr_state)
				A: // idle state
					begin
						if(start_conv)
							next_state <= B;
						else 
							next_state <= 0;
						clear <= 4'b0;
						increment <= 4'b0;
						wr_output_memory <= 0;
						start_PE <= 0;
						conv_complete <= 0;
						clear_mac <= 0;
						wr_input_memory <= 0;
					end
				B:	begin //load
						next_state <= D;
						clear <= 4'b0;
						increment <= 4'b0;
						wr_output_memory <= 1'b0;
						start_PE <= 1'b0;
						conv_complete <= 1'b0;
						clear_mac <= 1'b0;
						wr_input_memory <= 1'b1;
					end
				D: begin //start PE and wait for multiplicaton to be done (takes one clock cycle)
						if(r_status < R - 1 && s_status <= S - 1) // if r less than 2 and s less than 2 increment r
							begin
								increment <= 4'h8;
								clear <= 4'h0;
								next_state <= B;
							end
						else if(r_status == R-1 && s_status < S-1)
							begin
								increment <= 4'hC;
								clear <= 4'h8;
								next_state <= B;
							end
						else if(r_status == R-1 && s_status == S-1)
							begin
								clear <= 4'hC;
								increment <= 4'h0;
								next_state <= E;
							end
						else 
							begin
								clear <= 4'h0;
								increment <= 4'h0;
								next_state <= A;
							end
						wr_output_memory <= 1'b0;
						wr_input_memory <= 1'b0;
						start_PE <= 1'b1;
						conv_complete <= 1'b0;
						clear_mac <= 0;
					end
				E: begin
						increment <= 4'h2;
						wr_output_memory <= 1'b1; //writes output
						next_state <= G;
						wr_input_memory <= 1'b0;
						start_PE <= 1'b0;
						clear <= 4'b0;
						conv_complete <= 1'b0;
						clear_mac <= 4'b0;
					end
				G: begin
					if(w_status == W-R+1 && h_status < H-S)
						begin
							increment <= 4'h1;
							next_state <= I; //avoid race conditons with ADU by letting output become stable
						end
					else if(w_status == W-R+1 && h_status == H-S)
						begin
							next_state <= J;
							increment <= 4'h1;
						end
					else
						begin
							next_state <= B;
							increment <= 4'h0;
						end
					wr_output_memory <= 1'b0; 
					wr_input_memory <= 1'b0;
					start_PE <= 1'b0;
					conv_complete <= 1'b0;
					clear_mac <= 1'b1;
					clear <= 4'b0;
					end
				I: begin
					 	clear <= 4'h2;
						increment <= 4'b0;
						wr_output_memory <= 1'b0;
						wr_input_memory <= 1'b0;
						start_PE <= 1'b0;
						conv_complete <= 1'b0;
						next_state <= B;
						clear_mac <= 1'b0;
					end
				J:
					begin
						clear <= 4'hF;
						increment <= 4'b0;
						wr_output_memory <= 1'b0;
						wr_input_memory <= 1'b0;
						start_PE <= 1'b0;
						conv_complete <= 1'b1;
						next_state <= A;
						clear_mac <= 1'b0;
					end
				default: begin					
						next_state <= A;
						clear <= 4'b0;
						increment <= 4'b0;
						wr_output_memory <= 1'b0;
						start_PE <= 1'b0;
						conv_complete <= 1'b0;
						clear_mac <= 1'b0;
						wr_input_memory <= 1'b0;
					end
		endcase
	end
		
	always @ (posedge clk) //next state logic
		begin
			if(reset) curr_state <= A;
			else curr_state <= next_state;
		end
		
endmodule
