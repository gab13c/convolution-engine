/* ----------------------------------------------------------------
* This module is the Testbench for the MAC of the process engine.
* The inputs will be the weight and pixel value from the memory.
* The inputs will be multiplied and then summed 9 times forming the
* complete kernel and the output will be the summed subframe of [15:0] bits.
 
* @author - Nathan Kendall
* @date - 10/19/19
*---------------------------------------------------------------- */

module StatusReg_tb();
	
	localparam clk_cyc = 10; 

	localparam w_length = 24; 
	localparam h_length = 24; 
	localparam r_length = 3; 
	localparam s_length = 3; 
	
	reg [4:0] i = 5'b0;
	reg [4:0] j = 5'b0;
	reg [1:0] k = 2'b0;
	reg [1:0] l = 2'b0;
	
	reg clk = 1'b0;
	reg reset = 1'b0;
	reg [3:0] inc = 4'b0;
	reg [3:0] clear = 4'b0;
	wire [4:0] w;
	wire [4:0] h;
	wire [1:0] r;
	wire [1:0] s;
	
	StatusReg uut (
		.clk(clk), 
		.reset(reset), 
		.inc(inc), 
		.clear(clear), 
		.w(w), 
		.h(h), 
		.r(r), 
		.s(s) 
	);	 

	always
		#(clk_cyc) clk <= !clk;
	
	initial begin
		reset <= 1;
		#5;		
		reset <= 0;

		@(posedge clk);
		for(i=5'b0;i<h_length;i=i+1)					// Height Loop
		begin
			@(posedge clk);
			clear <= 4'b0000;
			inc <= 4'b0000;
			for(j=5'b0;j<w_length;j=j+1)				// Length Loop
			begin
				@(posedge clk);
				clear <= 4'b0000;
				inc <= 4'b0000;
				for(k=2'b0;k<s_length;k=k+1)			// Kernel Height Loop
				begin
					@(posedge clk);
					clear <= 4'b0000;
					inc <= 4'b0000;
					for(l=2'b0;l<r_length;l=l+1)		// Kernel Length Loop
					begin
						@(posedge clk);
						inc <= 4'b1000;
						@(posedge clk);
						inc <= 4'b0000;
					end
					@(posedge clk);
					clear = 4'b1000;
					inc = 4'b0100;
				end
				@(posedge clk);
				clear <= 4'b0100;
				inc <= 4'b0010;
			end
			@(posedge clk);
			clear <= 4'b0010;
			inc <= 4'b0001;
		end
		clear <= 4'b0000;
		inc <= 4'b0000;
//		$finish;
	end
endmodule
