/* ----------------------------------------------------------------
* This module is the Testbench for the MAC of the process engine.
* The inputs will be the weight and pixel value from the memory.
* The inputs will be multiplied and then summed 9 times forming the
* complete kernel and the output will be the summed subframe of [15:0] bits.
 
* @author - Nathan Kendall
* @date - 11/19/19
*---------------------------------------------------------------- */

module MAC_tb();
	
	localparam clk_cyc = 10;

	localparam w_length = 24; 
	localparam h_length = 24; 
	localparam r_length = 3; 
	localparam s_length = 3; 
	
	reg [4:0] i = 5'b0;
	reg [4:0] j = 5'b0;
	reg [1:0] k = 2'b0;
	reg [1:0] l = 2'b0;

	reg clk = 1'b0;
	reg reset = 1'b0;
	reg [7:0] pix_val = 1'b0;
	reg [7:0] weight = 1'b0;
	reg start = 1'b0;
	wire [15:0] subframe_out;
	
	MAC uut (
		.clk(clk), 
		.reset(reset), 
		.pix_val(pix_val), 
		.weight(weight), 
		.start(start), 
		.subframe_out(subframe_out)	
	);	 

	always
		#(clk_cyc) clk <= !clk;
	
	initial begin
	reset <= 1;
	#5;		
	reset <= 0;
	@(posedge clk);
	for(i=5'b0;i<h_length;i=i+1)					// Height Loop
	begin
		for(j=5'b0;j<w_length;j=j+1)				// Length Loop
		begin
			@(posedge clk);
			reset <= 1;
			@(posedge clk);
			reset <= 0;					
			for(k=2'b0;k<s_length;k=k+1)			// Kernel Height Loop
			begin
				for(l=2'b0;l<r_length;l=l+1)		// Kernel Length Loop
				begin
					@(posedge clk);
					pix_val = 180+l+k+j+i;
					weight = 5-l;
					@(posedge clk);
					start <= 1;
					@(posedge clk);
					start <= 0;					
				end
			end
		end
	end
//	$finish;
	end
endmodule
