vsim -novopt work.ucell_tb
# vsim work.ucell_tb
add wave -position insertpoint sim:/ucell_tb/*
# bp mac.v 31
# bp mac.v 31  ;# 1
# bp mac.v 24  ;# 2
# bp FSM.v 82  ;# 3
# bp SRAM.v 32  ;# 4
# bp FSM.v 73  ;# 5
# bp FSM.v 59  ;# 6
# bp SRAM.v 32 {echo [exa addr] ; echo [exa {SRAM[addr-1]}] } # -cond {addr == 0}
bp ucell_tb.v 101
run 10000
examine {/top_level/outputs/SRAM[0]}
echo "should print 582, which is conv output of first cell"